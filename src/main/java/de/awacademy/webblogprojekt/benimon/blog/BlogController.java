package de.awacademy.webblogprojekt.benimon.blog;

import de.awacademy.webblogprojekt.benimon.article.ArticleRepository;
import de.awacademy.webblogprojekt.benimon.category.CategoryDTO;
import de.awacademy.webblogprojekt.benimon.category.CategoryEntity;
import de.awacademy.webblogprojekt.benimon.category.CategoryRepository;
import de.awacademy.webblogprojekt.benimon.comment.CommentDTO;
import de.awacademy.webblogprojekt.benimon.comment.CommentRepository;
import de.awacademy.webblogprojekt.benimon.session.SessionRepository;
import de.awacademy.webblogprojekt.benimon.userRole.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class BlogController {
    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    SessionRepository sessionRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping("/")
    public String blog(Model model) {


        model.addAttribute("articles", articleRepository.findAllByOrderByCreationDateDesc());
        model.addAttribute("roles", userRoleRepository.findAll());
        model.addAttribute("comments", commentRepository.findAllByOrderByCreationDateAsc());
        model.addAttribute("categories", categoryRepository.findAll());

        model.addAttribute("comment", new CommentDTO());
        model.addAttribute("category", new CategoryDTO());

        return "blog";
    }

    @GetMapping("/filterCategory")
    public String readCategory(Model model, @RequestParam("categoryName") String name) {


        List<CategoryEntity> categoryEntityList = new ArrayList<CategoryEntity>() {
        };

        categoryEntityList.add(categoryRepository.findByCategoryName(name));

        model.addAttribute("articles", articleRepository.findAllByCategoryEntityListOrderByCreationDateDesc(categoryEntityList));
        model.addAttribute("roles", userRoleRepository.findAll());
        model.addAttribute("comments", commentRepository.findAllByOrderByCreationDateAsc());
        model.addAttribute("categories", categoryRepository.findAll());

        model.addAttribute("comment", new CommentDTO());
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setCategoryName(name);
        model.addAttribute("category", categoryDTO);

        return "blog";
    }

    @GetMapping("/filterUser")
    public String filterUser(Model model, @RequestParam("userName") String name) {


        model.addAttribute("articles", articleRepository.findAllByUserEntityUserNameOrderByCreationDateDesc(name));
        model.addAttribute("roles", userRoleRepository.findAll());
        model.addAttribute("comments", commentRepository.findAllByOrderByCreationDateAsc());
        model.addAttribute("categories", categoryRepository.findAll());

        model.addAttribute("comment", new CommentDTO());

        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setCategoryName(name);
        model.addAttribute("category", categoryDTO);
        return "blog";
    }

}
