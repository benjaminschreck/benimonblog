package de.awacademy.webblogprojekt.benimon.archive;

import de.awacademy.webblogprojekt.benimon.article.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ArchiveController {
    @Autowired
    ArchiveRepository archiveRepository;
    @Autowired
    ArticleRepository articleRepository;

    @GetMapping("/articlearchive")
    public String show(Model model, @RequestParam("articleId") long articleId) {
        model.addAttribute("article", articleRepository.findById(articleId));
        model.addAttribute("articlearchive", archiveRepository.findByArticleEntityOrderByCreationDateDesc(articleRepository.findById(articleId)));
        return "/articlearchive";
    }
}
