package de.awacademy.webblogprojekt.benimon.archive;

import de.awacademy.webblogprojekt.benimon.article.ArticleEntity;
import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Entity
public class ArchiveEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Type(type = "text")
    private String archiveText;

    private Instant creationDate;

    private String headline;

    @ManyToOne
    private ArticleEntity articleEntity;

    @ManyToOne
    private UserEntity userEntity;

    public ArchiveEntity() {
    }

    public ArchiveEntity(String archiveText, Instant creationDate, String headline, ArticleEntity articleEntity, UserEntity userEntity) {
        this.archiveText = archiveText;
        this.creationDate = creationDate;
        this.headline = headline;
        this.articleEntity = articleEntity;
        this.userEntity = userEntity;
    }

    public String getUser() {
        return userEntity.getUserName();
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public String getArchiveText() {
        return archiveText;
    }

    public String getCreationDate() {

        return LocalDateTime.ofInstant(creationDate, ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
    }

    public String getHeadline() {
        return headline;
    }

    public ArticleEntity getArticleEntity() {
        return articleEntity;
    }
}
