package de.awacademy.webblogprojekt.benimon.category;

import de.awacademy.webblogprojekt.benimon.user.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;


    @GetMapping("/category")
    public String create(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        return ("redirect:/");
    }

    @PostMapping("/category")
    public String create(@ModelAttribute("category") CategoryDTO categoryDTO, @ModelAttribute("currentUser") UserEntity currentUser) {
        if (currentUser == null || !currentUser.isAdmin()) {
            return "redirect:/";
        }
        CategoryEntity categoryEntity = new CategoryEntity(categoryDTO.getCategoryName());
        categoryRepository.save(categoryEntity);
        return ("redirect:/");
    }


}
