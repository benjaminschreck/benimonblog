package de.awacademy.webblogprojekt.benimon.user;

import de.awacademy.webblogprojekt.benimon.article.ArticleEntity;
import de.awacademy.webblogprojekt.benimon.comment.CommentEntity;
import de.awacademy.webblogprojekt.benimon.userRole.UserRoleEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String userName;
    private String password;

    @OneToOne
    private UserRoleEntity role;

    @OneToMany(mappedBy = "userEntity")
    private List<ArticleEntity> articleEntityList;

    @OneToMany(mappedBy = "userEntity")
    private List<CommentEntity> commentEntityList;

    public UserEntity() {
    }

    public UserEntity(String userName, String password, UserRoleEntity role) {
        this.userName = userName;
        this.password = password;
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role.getRoleName();
    }

    public void setRole(UserRoleEntity role) {
        this.role = role;
    }

    public boolean isAdmin() {
        if (role.getRoleName().equals("Admin")) {
            return true;
        } else {
            return false;
        }
    }

    public long getId() {
        return id;
    }

    public List<ArticleEntity> getArticleEntityList() {
        return articleEntityList;
    }

    public List<CommentEntity> getCommentEntityList() {
        return commentEntityList;
    }
}
