package de.awacademy.webblogprojekt.benimon.user;

import de.awacademy.webblogprojekt.benimon.userRole.UserRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findFirstByUserNameAndPassword(String userName, String password);

    UserEntity findByUserName(String userName);

    boolean existsByUserName(String UserName);

    UserEntity findById(long Id);

    List<UserEntity> findByRole(UserRoleEntity userRoleEntity);
}
